const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
    origin  : "*"
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({extended : true}));

app.get("/",(req,res)=>{
    res.json({
        message : "สวัสนี่คือการทำ Rest API นะ"
    });
}); 
const route_version = '/api/v1'

require("./app/routes/covid.route")(app,route_version);

const PORT = process.env.PORT || 8081;

app.listen(PORT,()=>{
    console.log(`Server is running or port ${PORT}.`);
});