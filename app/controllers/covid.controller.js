
const axios = require("axios");


exports.getReportCovidNewCasePerDateByDate = async (req,res) => {
    let date  = req.query.date;

    let res_data_covid  = await axios.get('https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-all');
    let filter_data_covid =  res_data_covid.data.find(data => data.txn_date == date );

    res.send({value : filter_data_covid});
}

exports.getReportCovidNewCasePerDateInMonthByMonthAndYear = async (req,res) => {
    let {year,month} = req.query;
    //let month = req.query.month;
    let start_date = new Date(Date.UTC(year,month-1,1,0,0,0,0));
    let end_date = new Date(Date.UTC(year,month,0,0,0,0,0));

    let res_data_covid  = await axios.get('https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-all');

    let filter_new_case_rage_date = res_data_covid.data.map(data => { 
        data.txn_date_format = new Date(data.txn_date); 
        const [year,month,day] = data.txn_date.split('-')
        data.x = `${day}/${month}/${(parseInt(year)+543)}`  ;
        data.y = data.new_case;
        return data;
     }).filter(data => data.txn_date_format >= start_date && data.txn_date_format <= end_date );


    res.send({
        start_date : start_date,
        end_date : end_date,
        value : filter_new_case_rage_date
    });
}

exports.getReportCovidNewCaseTop5ProvinceByDate = async (req,res) => {
    let date = req.query.date;

    let res_data_covid_province  = await axios.get('https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-by-provinces');
    let filter_province_by_date = res_data_covid_province.data.filter(data => data.txn_date == date);
    let sort_top_5 = filter_province_by_date.sort((obj1,obj2)=>{
            return obj2.new_case - obj1.new_case ; 
    }).slice(0,5) ;
    res.send({
        value : sort_top_5
    });
}

